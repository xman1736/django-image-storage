from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from storage.models.image import Image


class HasFileFilter(admin.SimpleListFilter):
    title = _('Has file')
    parameter_name = 'has_file'

    def lookups(self, request, model_admin):
        return (
            ('True', _('Yes')),
            ('False', _('No')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(file__isnull=False)
        elif self.value() == 'False':
            return queryset.filter(file__isnull=True)


class ImageAdmin(admin.ModelAdmin):
    list_display = ('get_name', 'has_file', 'created_at', 'is_temp',)

    search_fields = ('get_name', 'user_id',)
    list_filter = (HasFileFilter, 'created_at', 'is_temp',)

    readonly_fields = ('has_file', 'created_at',)

    fieldsets = (
        (_("Main params"), {
            'fields': ('user_id', 'is_temp',),
        }),
        (_("Prompts"), {
            'fields': ('positive_prompt', 'negative_prompt',),
        }),
        (_("File"), {
            'fields': ('file',),
        }),
        (_("Dates"), {
            'fields': ('created_at',),
        }),
    )

    def has_file(self, obj):
        return obj.has_file

    has_file.short_description = _('Has file')

    def get_name(self, obj):
        return obj.name

    get_name.short_description = _('Name')


admin.site.register(Image, ImageAdmin)
