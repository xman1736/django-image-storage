import hashlib
import hmac
import json
from urllib.parse import urljoin

import requests
from constance import config
from django.conf import settings
from rest_framework.permissions import BasePermission


class IsValidJWTTokenPermission(BasePermission):
    def has_permission(self, request, view):
        jwt_token = request.headers.get('Authorization', '').split('Bearer ')[-1]
        user_id = request.query_params.get('user_id', request.data.get('user_id', None))

        if jwt_token and user_id:
            auth_service_url = urljoin(config.URL_TO_AUTH_SERVICE, 'api/validity/')

            message = json.dumps({}, separators=(',', ':')).encode('utf-8')
            calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()
            headers = {'Authorization': f'HMAC {calculated_hmac}'}

            response = requests.get(auth_service_url, params={'jwt_token': jwt_token, 'user_id': user_id}, headers=headers)

            is_valid = response.json()['is_valid']
            return is_valid
        return False
