import logging
import os

from django.apps import AppConfig
from django.conf import settings
from django.utils.translation import gettext_lazy as _


class StorageConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "storage"
    verbose_name = _("Storage")

    def ready(self):
        if not os.path.isdir(settings.STATIC_ROOT):
            os.makedirs(settings.STATIC_ROOT)
        if not os.path.isdir(settings.MEDIA_ROOT):
            os.makedirs(settings.MEDIA_ROOT)


class ConstanceConfig(AppConfig):
    name = 'constance'
    verbose_name = _("Constance")
    logger = logging.getLogger('constance')
