import hashlib
import hmac
import json
from urllib.parse import urljoin

import requests
from constance import config
from django.conf import settings
from django.db import models
from django.utils import timezone

from django.utils.translation import gettext_lazy as _


class Image(models.Model):
    DEFAULT_NEGATIVE_PROMPT = "(worst quality:2), (low quality:2), (normal quality:2), lowres, ((monochrome)), ((grayscale)), bad anatomy, bad hands, mutated, deformed, bad proportions, blurry, distorted, text, signature, logo, username, (mosaic censoring, censored, bar censor), (embedding:badhandv4:1), (embedding:EasyNegative:1), (embedding:EasyNegativeV2:1), signature, artist name, text"

    GET_TOKEN_URL = urljoin(config.URL_TO_AUTH_SERVICE, 'api/get-token/')
    IS_SUBSCRIBED_URL = urljoin(config.URL_TO_SUBSCRIBE_SERVICE, 'api/is_subscribed/')

    user_id = models.PositiveIntegerField(
        verbose_name=_('User ID'),
        blank=False,
        null=False
    )

    file = models.ImageField(
        verbose_name=_('File'),
        upload_to='images/%Y/%m/%d/%H/%M/%S/',
        blank=True,
        null=True
    )

    positive_prompt = models.CharField(
        verbose_name=_('Positive prompt'),
        max_length=500,
        blank=False,
        null=False
    )

    negative_prompt = models.CharField(
        verbose_name=_('Negative prompt'),
        max_length=500,
        default=DEFAULT_NEGATIVE_PROMPT,
    )

    created_at = models.DateTimeField(
        verbose_name=_('Created at'),
        default=timezone.now
    )

    is_temp = models.BooleanField(
        verbose_name=_('Is temporary'),
        default=True,
        db_index=True
    )

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')

    def get_name(self) -> str:
        return f'User {self.user_id} --> {self.file.name if self.file else _("No file")}'

    @property
    def name(self) -> str:
        return self.get_name()

    @property
    def has_file(self) -> bool:
        return self.file is not None

    def get_token(self) -> str:
        data = {
            'user_id': self.user_id
        }

        message = json.dumps(data, separators=(',', ':')).encode('utf-8')
        calculated_hmac = hmac.new(settings.HMAC_KEY, message, hashlib.sha256).hexdigest()

        headers = {'Authorization': f'HMAC {calculated_hmac}'}
        response = requests.post(self.GET_TOKEN_URL, json=data, headers=headers)

        return response.json()['token']

    @property
    def is_premium(self) -> bool:
        headers = {'Authorization': f'Bearer {self.token}'}
        response = requests.get(self.IS_SUBSCRIBED_URL, params={"user_id": self.user_id}, headers=headers)
        return response.json().get('is_subscribed')

    @property
    def token(self):
        return self.get_token()

    def __str__(self):
        return self.name
