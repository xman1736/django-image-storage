from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter
from rest_framework.exceptions import ValidationError

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from storage.rest.permissions.jwt import IsValidJWTTokenPermission

from django_filters.rest_framework import DjangoFilterBackend

from storage.models.image import Image
from storage.rest.serializers.image import ImageSerializer


@swagger_auto_schema(
    operation_summary="List and create images",
    operation_description="List and create images based on user_id and is_temp filter. "
                          "If the requester's user_id matches the viewed_user_id, "
                          "they can see temporary images. Otherwise, only non-temporary images are shown.",
    manual_parameters=[
        openapi.Parameter('user_id', openapi.IN_PATH, description="User ID", type=openapi.TYPE_INTEGER),
        openapi.Parameter('is_temp', openapi.IN_QUERY, description="Filter images by temporary status",
                          type=openapi.TYPE_BOOLEAN),
    ],
    responses={
        200: openapi.Response(description="OK", schema=ImageSerializer(many=True)),
        201: openapi.Response(description="Created", schema=ImageSerializer),
        400: "Bad Request",
    },
)
class ImageViewSet(generics.ListCreateAPIView):
    serializer_class = ImageSerializer
    permission_classes = [IsValidJWTTokenPermission]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['is_temp']
    ordering_fields = ['created_at']

    def get_queryset(self):
        viewed_user_id = self.kwargs.get('user_id', None)
        user_id = self.get_user_id()
        is_temp = self.request.query_params.get('is_temp', None)

        if str(viewed_user_id) == user_id:
            if is_temp is not None:
                return Image.objects.filter(user_id=viewed_user_id, is_temp=is_temp)
            return Image.objects.filter(user_id=viewed_user_id)
        else:
            return Image.objects.filter(user_id=viewed_user_id, is_temp=False)

    def perform_create(self, serializer):
        user_id = self.get_user_id()
        serializer.save(user_id=user_id)

    def get_user_id(self):
        user_id = self.request.data.get('user_id', None)
        if not user_id:
            user_id = self.request.query_params.get('user_id', None)
        if not user_id:
            raise ValidationError("User ID is required. [In query or body]", code='invalid')
        return user_id

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.filter_queryset(self.get_queryset())

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        except ValidationError as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@swagger_auto_schema(
    operation_summary="Retrieve, update, or delete an image",
    operation_description="Retrieve, update, or delete an image based on user_id and image ID. "
                          "If the image is temporary or the requester's user_id matches the image's user_id, "
                          "the image can be deleted. Otherwise, a 403 Forbidden response is returned.",
    manual_parameters=[
        openapi.Parameter('user_id', openapi.IN_QUERY, description="User ID", type=openapi.TYPE_INTEGER),
        openapi.Parameter('is_temp', openapi.IN_QUERY, description="Filter images by temporary status",
                          type=openapi.TYPE_BOOLEAN),
    ],
    responses={
        200: openapi.Response(description="OK", schema=ImageSerializer),
        403: "Forbidden",
        404: "Not Found",
        400: "Bad Request",
    },
)
class ImageDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ImageSerializer
    permission_classes = [IsValidJWTTokenPermission]

    def get_queryset(self):
        viewed_user_id = self.kwargs.get('user_id', None)
        user_id = self.get_user_id()
        is_temp = self.request.query_params.get('is_temp', None)

        if str(viewed_user_id) == user_id:
            if is_temp is not None:
                return Image.objects.filter(user_id=viewed_user_id, is_temp=is_temp)
            return Image.objects.filter(user_id=viewed_user_id)
        else:
            return Image.objects.filter(user_id=viewed_user_id, is_temp=False)

    def perform_destroy(self, instance):
        user_id = self.get_user_id()
        if instance.is_temp or instance.user_id == user_id:
            instance.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({"detail": "You don't have permission to delete this image."},
                            status=status.HTTP_403_FORBIDDEN)

    def get_user_id(self):
        user_id = self.request.data.get('user_id', None)
        if not user_id:
            user_id = self.request.query_params.get('user_id', None)
        if not user_id:
            raise ValidationError("User ID is required. [In query or body]", code='invalid')
        return user_id

    def retrieve(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        except ValidationError as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
