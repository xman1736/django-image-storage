from django.urls import path, include
from storage.views.image import ImageViewSet, ImageDetailView


urlpatterns = [
    path('images/', ImageViewSet.as_view(), name='image-list'),
    path('images/<int:user_id>/', ImageViewSet.as_view(), name='image-list-user-specific'),
    path('images/<int:user_id>/<int:pk>/', ImageDetailView.as_view(), name='image-detail'),
]