import re

import os


from django.core.management import BaseCommand
from django.conf import settings

from storage.models.image import Image


class Command(BaseCommand):
    help = 'Test'

    def handle(self, *args, **kwargs):
        Image.objects.get(id=7).delete()

