import base64
import logging
from io import BytesIO
from urllib.parse import urljoin
import requests

from asgiref.sync import sync_to_async
from django.core.files.base import File
from django.utils import timezone

from .base_subprocess import Command as BaseCommand

from storage.models.image import Image

from constance import config

error_logger = logging.getLogger('logfile_error')


class ImageFetcher(object):
    def __init__(self):
        self.generate_service_url = urljoin(config.URL_TO_IMAGE_GENERATOR, 'api/generate/')

    def processing(self, image: Image):
        data = {
            'user_id': image.user_id,
            'positive_prompt': image.positive_prompt,
            'negative_prompt': image.negative_prompt
        }

        headers = {'Authorization': f'Bearer {image.token}'}
        response = requests.post(self.generate_service_url, json=data, headers=headers)
        response_json = response.json()

        success = response_json.get('success', True)
        if not success:
            error_logger.error(f"Error while fetching image: {response_json}")
            return

        image_base64 = response_json.get('image', None)
        if not image_base64:
            error_logger.error(f"Response without image: {image.id}")
            return

        image_bytes = base64.b64decode(image_base64)
        image_io = BytesIO(image_bytes)

        img_name = f'{image.id}_{image.user_id}.png'

        image.file.save(
            img_name,
            File(file=image_io),
            save=True
        )
        image.created_at = timezone.now()
        image.save()


class Command(BaseCommand):
    help = 'Check Image objects and fetch image if file is None'

    command_name = __name__.rsplit('.', 1)[-1]

    delay = 60

    max_images = 5

    image_fetcher = None

    @sync_to_async
    def processing(self):
        images = Image.objects.filter(file__exact='')[:self.max_images]
        for image in images:
            self.image_fetcher.processing(image)

    def before_start(self):
        self.image_fetcher = ImageFetcher()

    async def do_action(self, context):
        await self.processing()
