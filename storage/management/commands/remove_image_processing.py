from datetime import timedelta
from urllib.parse import urljoin

import requests
from asgiref.sync import sync_to_async
from constance import config

from .base_subprocess import Command as BaseCommand
from django.utils import timezone

from storage.models.image import Image


class ImageRemover(object):

    def processing(self, image: Image):
        if image.is_premium:
           return

        if image.created_at + timedelta(hours=config.REMOVE_TEMP_IMAGES_HOURS) < timezone.now():
            image.delete()


class Command(BaseCommand):
    help = 'Check Image objects and fetch image if file is None'

    command_name = __name__.rsplit('.', 1)[-1]

    delay = 60

    max_images = 50

    image_remover = None

    @sync_to_async
    def processing(self):
        images = Image.objects.filter(is_temp=True).exclude(file__exact='')[:self.max_images]
        for image in images:
            self.image_remover.processing(image)

    def before_start(self):
        self.image_remover = ImageRemover()

    async def do_action(self, context):
        await self.processing()
