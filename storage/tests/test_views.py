from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from storage.models.image import Image


class ImageViewSetTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.image = Image.objects.create(user_id=10000, positive_prompt='test', negative_prompt='')

    def test_list_images(self):
        url = '/api/images/{}/?user_id={}'.format(self.image.user_id, self.image.user_id)
        response = self.client.get(url, HTTP_AUTHORIZATION=f'Bearer {self.image.token}')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_image(self):
        url = '/api/images/{}/?user_id={}'.format(self.image.user_id, self.image.user_id)
        data = {'user_id': self.image.user_id, 'positive_prompt': 'positive', 'negative_prompt': 'negative'}
        response = self.client.post(url, data, HTTP_AUTHORIZATION=f'Bearer {self.image.token}')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ImageDetailViewTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.image = Image.objects.create(user_id=1000010, positive_prompt='positive', negative_prompt='negative')

    def test_retrieve_image(self):
        url = '/api/images/{}/{}/?user_id={}'.format(self.image.user_id, self.image.id, self.image.user_id)
        response = self.client.get(url, HTTP_AUTHORIZATION=f'Bearer {self.image.token}')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_image(self):
        url = '/api/images/{}/{}/?user_id={}'.format(self.image.user_id, self.image.id, self.image.user_id)
        response = self.client.delete(url, HTTP_AUTHORIZATION=f'Bearer {self.image.token}')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
